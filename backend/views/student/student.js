$(document).ready(() => {
  getStudent();
  // 抓取學生資料
  function getStudent() {
    $.ajax({
      url: 'http://127.0.0.1:4000/student',
      method: 'GET',
      success: (res) => {
        // 先將table清空
        $('table').empty();
        const data = res.data;
        // 渲染table的header
        $('table').append(`
          <thead>
          <tr>
          <th style="width: 10%;" scope="col">ID</th>
          <th style="width: 30%;" scope="col">學號</th>
          <th style="width: 45%;" scope="col">Name</th>
          <th style="width: 15%;" scope="col">操作</th>
          </tr>
          </thead>
          `);
        // 將學生資料渲染到頁面上
        data.forEach((item) => {
          $('table').append(`
            <tr>
            <td>${item.id}</td>
            <td>${item.number}</td>
            <td>${item.name}</td>
            <td>
            <button type="button"  data-bs-toggle="modal" data-bs-target="#editModal" class="editBtn btn btn-outline-primary" data-id="${item.id}" data-number="${item.number}" data-name="${item.name}">編輯</button>
            <button type="button" class="deleteBtn btn btn-outline-primary" data-id="${item.id}">刪除</button>
            </td>
            </tr>
            `);
        });
      },
    });
  }

  // 新增學生資料
  function postStudent(number, name) {
    $.ajax({
      url: 'http://127.0.0.1:4000/student',
      method: 'POST',
      contentType: 'application/json',
      data: JSON.stringify({
        number: number,
        name: name,
      }),
      success: (res) => {
        if (res.statusCode === 201) {
          $('#idIpt').val('');
          $('#nameIpt').val('');
          getStudent();
          alert('新增成功');
        } else {
          alert(`
            新增失敗,因為${res.message}
            `);
        }
      },
    });
  }

  // 新增按鈕點擊事件
  $('.addBtn').click(function () {
    const number = $('#idIpt').val();
    const name = $('#nameIpt').val();
    if (number === '' || name === '') {
      alert('學號或名稱尚未輸入');
      return;
    }
    postStudent(parseInt(number), name);
  });

  // 修改學生資料
  function editStudent(id, number, name) {
    $.ajax({
      url: 'http://127.0.0.1:4000/student',
      method: 'PATCH',
      contentType: 'application/json',
      data: JSON.stringify({
        id: id,
        number: number,
        name: name,
      }),
      success: (res) => {
        if (res.statusCode === 200) {
          getStudent();
        } else {
          alert(`
            修改失敗,因為${res.message}
            `);
        }
      },
    });
  }

  // 刪除學生資料
  function deleteStudent(id) {
    $.ajax({
      url: `http://127.0.0.1:4000/student/${id}`,
      method: 'DELETE',
      success: (res) => {
        if (res.statusCode === 200) {
          getStudent();
          alert('刪除成功');
        } else {
          alert(`
            刪除失敗,因為${res.message}
            `);
        }
      },
    });
  }
  // 刪除按鈕點擊事件
  $('table').on('click', '.deleteBtn', function () {
    const id = $(this).data('id');
    deleteStudent(id);
  });

  // 全域變數存放當前所選擇的學生資料id
  var currentId;
  // 全域變數存放當前所選擇的學生資料number
  var currentNumber;
  // 全域變數存放當前所選擇的學生資料name
  var currentName;
  // 打開編輯視窗點擊事件
  $('table').on('click', '.editBtn', function () {
    currentId = $(this).data('id');
    currentNumber = $(this).data('number');
    currentName = $(this).data('name');
    $('#editNumberIpt').val(currentNumber);
    $('#editNameIpt').val(currentName);
  });

  // 編輯完成保存按鈕點擊事件
  $('#saveBtn').click(function () {
    const number = $('#editNumberIpt').val();
    const name = $('#editNameIpt').val();
    // 如果學號或是姓名其中一項為空
    // 則跳出提示訊息學號或名稱不可為空
    if (number === '' || name === '') {
      alert('學號或名稱不可為空');
      return;
    }
    // 如果學號或姓名沒有更動
    // 則跳出提示訊息保存成功
    if (currentNumber == number && currentName == name) {
      $('#editModal').modal('hide');
      setTimeout(() => {
        alert('保存成功');
      }, 500);
      return;
    }
    // 如果學號或姓名有更動
    // 則跳出提示訊息修改成功
    editStudent(currentId, number, name);
    $('#editModal').modal('hide');
    setTimeout(() => {
      alert('修改成功');
    }, 500);
  });
});
