import { Controller, Get, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { Response } from 'express';
import { join } from 'path';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getIndex(@Res() res: Response): void {
    res.sendFile(join(__dirname, '..', 'views', 'index.html'));
  }

  @Get('system/student')
  getStudent(@Res() res: Response): void {
    res.sendFile(join(__dirname, '..', 'views', 'student', 'student.html'));
  }

  @Get('helloworld')
  getHelloworld(): string {
    return this.appService.getHelloworld();
  }
}
