import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from 'express';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.use(express.static(join(__dirname, '..', 'views')));
  app.use(express.static(join(__dirname, '..', 'views', 'student')));
  await app.listen(4000);
}
bootstrap();
