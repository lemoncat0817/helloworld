import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHelloworld(): string {
    return 'hello world';
  }
}
