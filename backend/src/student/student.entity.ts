import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class studentEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  number: number;

  @Column()
  name: string;
}
