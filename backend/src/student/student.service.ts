import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { studentEntity } from './student.entity';

@Injectable()
export class StudentService {
  constructor(
    @InjectRepository(studentEntity)
    private studentRepository: Repository<studentEntity>,
  ) {}

  getAllStudents(): Promise<studentEntity[]> {
    return this.studentRepository.find();
  }

  addStudent(number: number, name: string): Promise<studentEntity> {
    const newStudent = this.studentRepository.create({ number, name });
    return this.studentRepository.save(newStudent);
  }

  async updateStudent(
    id: number,
    number: number,
    name: string,
  ): Promise<studentEntity> {
    await this.studentRepository.update(id, { number, name });
    return this.studentRepository.findOne({ where: { id } });
  }

  async deleteStudent(id: number): Promise<boolean> {
    await this.studentRepository.delete(id);
    return true;
  }
}
