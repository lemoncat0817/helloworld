import {
  Controller,
  Get,
  Post,
  Patch,
  Delete,
  Body,
  Param,
  HttpStatus,
} from '@nestjs/common';
import { StudentService } from './student.service';

@Controller('student')
export class StudentController {
  constructor(private readonly studentService: StudentService) {}

  @Get()
  async getAllStudents() {
    const students = await this.studentService.getAllStudents();
    return {
      statusCode: HttpStatus.OK,
      message: '成功',
      success: true,
      data: students,
    };
  }

  @Post()
  async addStudent(@Body() body: { number: number; name: string }) {
    if (!body.number || !body.name) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        message: '參數錯誤',
        success: false,
        data: null,
      };
    }
    if (!(typeof body.number === 'number')) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        message: '參數錯誤',
        success: false,
        data: null,
      };
    }
    await this.studentService.addStudent(body.number, body.name);
    const students = await this.studentService.getAllStudents();
    return {
      statusCode: HttpStatus.CREATED,
      message: '成功',
      success: true,
      data: students,
    };
  }

  @Patch()
  async updateStudent(
    @Body() body: { id: number; number: number; name: string },
  ) {
    const student = await this.studentService.updateStudent(
      body.id,
      body.number,
      body.name,
    );
    if (!student) {
      return {
        statusCode: HttpStatus.NOT_FOUND,
        message: '學生未找到',
        success: false,
        data: null,
      };
    }

    const students = await this.studentService.getAllStudents();
    return {
      statusCode: HttpStatus.OK,
      message: '更新成功',
      success: true,
      data: students,
    };
  }

  @Delete(':id')
  async deleteStudent(@Param('id') id: number) {
    const result = await this.studentService.deleteStudent(id);
    const students = await this.studentService.getAllStudents();

    if (!result) {
      return {
        statusCode: HttpStatus.NOT_FOUND,
        message: '學生未找到',
        success: false,
        data: null,
      };
    }

    return {
      statusCode: HttpStatus.OK,
      message: '刪除成功',
      success: true,
      data: students,
    };
  }
}
