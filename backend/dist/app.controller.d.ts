import { AppService } from './app.service';
import { Response } from 'express';
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    getIndex(res: Response): void;
    getStudent(res: Response): void;
    getHelloworld(): string;
}
