import { HttpStatus } from '@nestjs/common';
import { StudentService } from './student.service';
export declare class StudentController {
    private readonly studentService;
    constructor(studentService: StudentService);
    getAllStudents(): Promise<{
        statusCode: HttpStatus;
        message: string;
        success: boolean;
        data: import("./student.entity").studentEntity[];
    }>;
    addStudent(body: {
        number: number;
        name: string;
    }): Promise<{
        statusCode: HttpStatus;
        message: string;
        success: boolean;
        data: import("./student.entity").studentEntity[];
    }>;
    updateStudent(body: {
        id: number;
        number: number;
        name: string;
    }): Promise<{
        statusCode: HttpStatus;
        message: string;
        success: boolean;
        data: import("./student.entity").studentEntity[];
    }>;
    deleteStudent(id: number): Promise<{
        statusCode: HttpStatus;
        message: string;
        success: boolean;
        data: import("./student.entity").studentEntity[];
    }>;
}
