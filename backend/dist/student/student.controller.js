"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StudentController = void 0;
const common_1 = require("@nestjs/common");
const student_service_1 = require("./student.service");
let StudentController = class StudentController {
    constructor(studentService) {
        this.studentService = studentService;
    }
    async getAllStudents() {
        const students = await this.studentService.getAllStudents();
        return {
            statusCode: common_1.HttpStatus.OK,
            message: '成功',
            success: true,
            data: students,
        };
    }
    async addStudent(body) {
        if (!body.number || !body.name) {
            return {
                statusCode: common_1.HttpStatus.BAD_REQUEST,
                message: '參數錯誤',
                success: false,
                data: null,
            };
        }
        if (!(typeof body.number === 'number')) {
            return {
                statusCode: common_1.HttpStatus.BAD_REQUEST,
                message: '參數錯誤',
                success: false,
                data: null,
            };
        }
        await this.studentService.addStudent(body.number, body.name);
        const students = await this.studentService.getAllStudents();
        return {
            statusCode: common_1.HttpStatus.CREATED,
            message: '成功',
            success: true,
            data: students,
        };
    }
    async updateStudent(body) {
        const student = await this.studentService.updateStudent(body.id, body.number, body.name);
        if (!student) {
            return {
                statusCode: common_1.HttpStatus.NOT_FOUND,
                message: '學生未找到',
                success: false,
                data: null,
            };
        }
        const students = await this.studentService.getAllStudents();
        return {
            statusCode: common_1.HttpStatus.OK,
            message: '更新成功',
            success: true,
            data: students,
        };
    }
    async deleteStudent(id) {
        const result = await this.studentService.deleteStudent(id);
        const students = await this.studentService.getAllStudents();
        if (!result) {
            return {
                statusCode: common_1.HttpStatus.NOT_FOUND,
                message: '學生未找到',
                success: false,
                data: null,
            };
        }
        return {
            statusCode: common_1.HttpStatus.OK,
            message: '刪除成功',
            success: true,
            data: students,
        };
    }
};
exports.StudentController = StudentController;
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], StudentController.prototype, "getAllStudents", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], StudentController.prototype, "addStudent", null);
__decorate([
    (0, common_1.Patch)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], StudentController.prototype, "updateStudent", null);
__decorate([
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], StudentController.prototype, "deleteStudent", null);
exports.StudentController = StudentController = __decorate([
    (0, common_1.Controller)('student'),
    __metadata("design:paramtypes", [student_service_1.StudentService])
], StudentController);
//# sourceMappingURL=student.controller.js.map