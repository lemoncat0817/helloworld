import { Repository } from 'typeorm';
import { studentEntity } from './student.entity';
export declare class StudentService {
    private studentRepository;
    constructor(studentRepository: Repository<studentEntity>);
    getAllStudents(): Promise<studentEntity[]>;
    addStudent(number: number, name: string): Promise<studentEntity>;
    updateStudent(id: number, number: number, name: string): Promise<studentEntity>;
    deleteStudent(id: number): Promise<boolean>;
}
